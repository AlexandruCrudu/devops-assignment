#!/bin/bash
#
# Add game data to the backend
#
# To clear the existing data please use the following command:
#   curl -X DELETE {url}
url='http://localhost:5000/games'
input_file="chess.pgn"

case "$1" in
# echo "press enter to continue"
# read temp

  post)

filename="chess.pgn"

event=""
site=""
date=""
round=""
white=""
black=""
result=""
whiteelo=""
blackelo=""
eco=""
moves=""

#!/bin/bash

while read -r line
do
    if [[ $line == "[Event"* ]]; then
        # Remove the opening and closing square brackets and the "Event " prefix
        value=$(echo "$line" | sed 's/\[\w\+\s\+//; s/\]//')
        event=$value
        # echo "$value"
    fi
    if [[ $line == "[Site"* ]]; then
        # Remove the opening and closing square brackets and the "Event " prefix
        value=$(echo "$line" | sed 's/\[\w\+\s\+//; s/\]//')
        site=$value
        # echo "$site"
    fi
    if [[ $line == "[Date"* ]]; then
        # Remove the opening and closing square brackets and the "Event " prefix
        value=$(echo "$line" | sed 's/\[\w\+\s\+//; s/\]//')
        date=$value
        # echo "$date"
    fi
    if [[ $line == "[Round"* ]]; then
        # Remove the opening and closing square brackets and the "Event " prefix
        value=$(echo "$line" | sed 's/\[\w\+\s\+//; s/\]//')
        round=$value
        # echo "$round"
    fi
    if [[ $line == "[White"* ]]; then
        # Remove the opening and closing square brackets and the "Event " prefix
        value=$(echo "$line" | sed 's/\[\w\+\s\+//; s/\]//')
        white=$value
        # echo "$white"
    fi
    if [[ $line == "[Black"* ]]; then
        # Remove the opening and closing square brackets and the "Event " prefix
        value=$(echo "$line" | sed 's/\[\w\+\s\+//; s/\]//')
        black=$value
        # echo "$black"
    fi
    if [[ $line == "[Result"* ]]; then
        # Remove the opening and closing square brackets and the "Event " prefix
        value=$(echo "$line" | sed 's/\[\w\+\s\+//; s/\]//')
        result=$value
        # echo "$result"
    fi
    if [[ $line == "[WhiteElo"* ]]; then
        # Remove the opening and closing square brackets and the "Event " prefix
        value=$(echo "$line" | sed 's/\[\w\+\s\+//; s/\]//')
        whiteelo=$value
        # echo "$whiteelo"
    fi
    if [[ $line == "[BlackElo"* ]]; then
        # Remove the opening and closing square brackets and the "Event " prefix
        value=$(echo "$line" | sed 's/\[\w\+\s\+//; s/\]//')
        blackelo=$value
        # echo "$blackelo"
    fi
    if [[ $line == "[ECO"* ]]; then
        # Remove the opening and closing square brackets and the "Event " prefix
        value=$(echo "$line" | sed 's/\[\w\+\s\+//; s/\]//')
        eco=$value
        # echo "$eco"
    fi
    if [[ $line == "1."* ]]; then
        # Remove the opening and closing square brackets and the "Event " prefix
        moves=$line


json='
{
    "type": "Game",
    "fields": {
        "Event": '"$event"',
        "Site": '"$site"',
        "Date": '"$date"',
        "Round": '"$round"',
        "White": '"$white"',
        "Black": '"$black"',
        "Result": '"$result"',
        "WhiteElo": '"$whiteelo"',
        "BlackElo": '"$blackelo"',
        "ECO": '"$eco"'
    },
    "moves": "'"$moves"'"
}'
# echo "$json"
curl -X POST -H "Content-Type: application/json" -d "$json" http://localhost:5000/games
read temp
    fi
done < "$filename"

read temp

    ;;

  delete)
    # Send a DELETE request to the backend
    curl -X DELETE $url
    ;;

  *)
    echo "Usage: $0 {post|delete}"
    exit 1
    ;;
esac
