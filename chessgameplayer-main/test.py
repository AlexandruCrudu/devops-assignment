import json
import requests

# Read the contents of the chess.pgn file
with open('chess.pgn', 'r') as f:
    content = f.read()

# Split the content into individual games based on the [Event tag
games = content.split('[Event ')

# Remove the empty first game (before the first [Event tag)
games.pop(0)

# Extract the game information and create a list of games
game_list = []
for game in games:
    # Split the game into individual lines
    lines = game.split('\n')
    # Extract the game information from the lines
    event = lines[0][1:-2]
    site = lines[1][7:-2]
    date = lines[2][7:-2]
    round = lines[3][7:-2]
    white = lines[4][7:-2]
    black = lines[5][7:-2]
    result = lines[6][7:-2]
    white_elo = lines[7][11:-2]
    black_elo = lines[8][11:-2]
    eco = lines[9][6:-2]
    moves = ' '.join(lines[10:])
    # Create a dictionary representing the game
    game_dict = {
        'event': event,
        'site': site,
        'date': date,
        'round': round,
        'white': white,
        'black': black,
        'result': result,
        'white_elo': white_elo,
        'black_elo': black_elo,
        'eco': eco,
        'moves': moves
    }
    # Add the game to the list of games
    game_list.append(game_dict)

# Send the list of games to the backend using a POST request
url = 'http://localhost:5000/games'
headers = {'Content-Type': 'application/json'}
response = requests.post(url, headers=headers, data=json.dumps(game_list))

# Print the response from the server
print(response.content)